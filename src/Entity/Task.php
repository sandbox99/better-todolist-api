<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @Groups({"readTodoList"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"readTodoList"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"readTodoList"})
     * @ORM\Column(type="boolean")
     */
    private $done = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TodoList", inversedBy="tasks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $todolist;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDone(): ?bool
    {
        return $this->done;
    }

    public function setDone(bool $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getTodolist(): ?TodoList
    {
        return $this->todolist;
    }

    public function setTodolist(?TodoList $todolist): self
    {
        $this->todolist = $todolist;

        return $this;
    }
}
